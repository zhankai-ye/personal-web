+++
title = "Home"
description = "Home site description."
sort_by = "date"
template = "index.html"
+++

{{ tab_nav_box(
        id = "home"
        class = "mb-5"
        tab_titles = [
            "👋 Hello",
            "📝 Experience",
            "📧 Contact"
        ]
        tab_contents = [
            "I am currently a first-year MS in Statistical Science candidate at <a href='https://stat.duke.edu/' target='_blank'>Duke University</a>. I obtained my BS (Hons) in Integrative Biomedical Science from <a href='https://www.ed.ac.uk' target='_blank'>the University of Edinburgh</a> and <a href='https://www.zju.edu.cn/english/' target='_blank'>Zhejiang University</a>.",
            "I gained robust intuitive and theoretical understanding of the foundations of Statistics/ML and I am experienced with data wrangling and modeling using SQL, Python, R, Julia, and SAS. Meanwhile, I am combined with biomedical domain knowledge and health insights from many health data science projects. I am motivated to harness machine learning for impactful healthcare solutions.",
            "Email: <a href='mailto:zhankai.ye@duke.edu'>zhankai.ye@duke.edu</a><br>Phone: (984)-999-5538"
        ]
    )
}}

## Other Link

[![DukeScholars](https://img.shields.io/badge/Duke-%40Scholars-blue "DukeScholars")](https://scholars.duke.edu/person/zhankai.ye)